import requests
import logging
import json
import os

from decimal import Decimal, getcontext

from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry

DEFAULT_RUNE_ASSET = "BNB.RUNE-A1F"


def get_rune_asset():
    return Asset(os.environ.get("RUNE", DEFAULT_RUNE_ASSET))


def requests_retry_session(
    retries=6, backoff_factor=1, status_forcelist=(500, 502, 504), session=None,
):
    """
    Creates a request session that has auto retry
    """
    session = session or requests.Session()
    retry = Retry(
        total=retries,
        read=retries,
        connect=retries,
        backoff_factor=backoff_factor,
        status_forcelist=status_forcelist,
    )
    adapter = HTTPAdapter(max_retries=retry)
    session.mount("http://", adapter)
    session.mount("https://", adapter)
    return session


def get_share(part, total, alloc):
    """
    Calculates the share of something
    (Allocation / (Total / part))
    """
    if total == 0 or part == 0:
        return 0
    getcontext().prec = 18
    return int(round(Decimal(alloc) / (Decimal(total) / Decimal(part))))


class HttpClient:
    """
    An generic http client
    """

    def __init__(self, base_url):
        self.base_url = base_url

    def get_url(self, path):
        """
        Get fully qualified url with given path
        """
        return self.base_url + path

    def fetch(self, path, args={}):
        """
        Make a get request
        """
        url = self.get_url(path)
        resp = requests_retry_session().get(url, params=args)
        resp.raise_for_status()
        return resp.json()

    def post(self, path, payload={}):
        """
        Make a post request
        """
        url = self.get_url(path)
        resp = requests_retry_session().post(url, json=payload)
        if resp.status_code != 200:
            logging.error(resp.text)
        resp.raise_for_status()
        return json.loads(resp.text, parse_float=Decimal)


class Jsonable:
    def to_json(self):
        return json.dumps(self, default=lambda x: x.__dict__)


class Asset(str, Jsonable):
    def __new__(cls, value, *args, **kwargs):
        if len(value.split(".")) < 2:
            value = f"BNB.{value}"  # default to thorchain
        return super().__new__(cls, value)

    def is_bnb(self):
        """
        Is this asset bnb?
        """
        return self.get_symbol().startswith("BNB")

    def is_btc(self):
        """
        Is this asset btc?
        """
        return self.get_symbol().startswith("BTC")

    def is_eth(self):
        """
        Is this asset eth?
        """
        return self.get_symbol().startswith("ETH")

    def is_rune(self):
        """
        Is this asset rune?
        """
        return self.get_symbol().startswith("RUNE")

    def get_symbol(self):
        """
        Return symbol part of the asset string
        """
        return self.split(".")[1]

    def get_ticker(self):
        """
        Return ticker part of the asset string
        """
        return self.get_symbol().split("-")[0]

    def get_chain(self):
        """
        Return chain part of the asset string
        """
        return self.split(".")[0]

    def prometheus_name(self):
        name = self
        name = name.replace(".", "_")
        return name.replace("-", "_")


class Coin(Jsonable):
    """
    A class that represents a coin and an amount
    """

    ONE = 1e8

    def __init__(self, asset, amount=0):
        self.asset = Asset(asset)
        self.amount = int(amount)

    def is_rune(self):
        """
        Is this coin rune?
        """
        return self.asset.is_rune()

    def is_zero(self):
        """
        Is the amount of this coin zero?
        """
        return self.amount == 0

    def to_thorchain_fmt(self):
        """
        Convert the class to an dictionary, specifically in a format for
        thorchain
        """
        return {
            "asset": self.asset,
            "amount": str(self.amount),
        }

    def to_binance_fmt(self):
        """
        Convert the class to an dictionary, specifically in a format for
        mock-binance
        """
        return {
            "denom": self.asset.get_symbol(),
            "amount": self.amount,
        }

    def __eq__(self, other):
        return self.asset == other.asset and self.amount == other.amount

    def __lt__(self, other):
        return self.amount < other.amount

    def __hash__(self):
        return hash(str(self))

    @classmethod
    def from_dict(cls, value):
        return cls(value["asset"], value["amount"])

    def __repr__(self):
        return f"<Coin {self.amount:0,.0f}_{self.asset}>"

    def __str__(self):
        return f"{float(self.amount) / 1e8}_{self.asset}"


class OrderBook:
    def __init__(self, asset=None, asks=[], bids=[]):
        self.asset = asset

        self.asks = asks
        self.bids = bids

    def ratio(self):
        count = 0
        total = 0
        if len(self.bids) > 0:
            count += 1
            total += self.bids[0].price

        if len(self.asks) > 0:
            count += 1
            total += self.asks[0].price

        if count == 0:
            return 0

        return 1 / (total / count)

    def market_buy_order(self, bnb_amt):
        """
        With given bnb amount, get as much of the asset that is available
        Returns amount of asset, and BNB cost
        """
        bnb_amt /= 1e8
        asset_amt = 0
        cost = 0
        price = 0
        for ask in self.asks:
            if ask.cost() + cost >= bnb_amt:
                asset_amt += (bnb_amt - cost) / ask.price
                cost = bnb_amt
                price = ask.price
                break
            asset_amt += ask.amount
            cost += ask.cost()

        return asset_amt * 1e8, cost * 1e8, price

    def market_sell_order(self, asset_amt):
        """
        With given asset amount, get as much of BNB that is available
        Returns amount of BNB, and asset cost
        """
        asset_amt /= 1e8
        bnb_amt = 0
        cost = 0
        price = 0
        for i, bid in enumerate(self.bids):
            if i == 0:
                price = bid.price
            if bid.amount + cost >= asset_amt:
                bnb_amt += (asset_amt - cost) * bid.price
                cost = asset_amt
                break
            bnb_amt += bid.cost()
            cost += bid.amount

        return bnb_amt * 1e8, cost * 1e8, price

    @classmethod
    def from_binance_dex_json(cls, data):
        asks = [Order.from_binance_dex_json(ask_data) for ask_data in data["asks"]]
        bids = [Order.from_binance_dex_json(bid_data) for bid_data in data["bids"]]
        return OrderBook(None, asks, bids)


class Order:
    def __init__(self, amount, price, source=None):
        self.source = source
        self.amount = float(amount)
        self.price = float(price)

    def get(self, amt):
        return min(amt, self.amount)

    def cost(self, amt=None):
        if amt is None:
            amt = self.amount
        return amt * self.price

    def reverse_cost(self, amt=None):
        if amt is None:
            return self.cost()
        return amt / self.price

    @classmethod
    def from_binance_dex_json(cls, data):
        return Order(data[1], data[0], "BinanceDex")

    def __eq__(self, other):
        return self.price == other.price

    def __lt__(self, other):
        return self.price < other.price

    def __repr__(self):
        return f"<Order {self.source} {self.amount} * {self.price} = {self.cost()}>"

    def __str__(self):
        return f"Order {self.source} {self.amount} * {self.price} = {self.cost()}"

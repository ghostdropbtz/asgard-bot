#!/usr/bin/env python

import os
import logging

# Init logging
logging.basicConfig(
    format="%(asctime)s | %(levelname).4s | %(message)s",
    level=os.environ.get("LOGLEVEL", "INFO"),
)


def str2bool(v):
    return v.lower() in ("yes", "true", "t", "1")


class Configuration:
    def __init__(self):
        self.thorchain = os.getenv("THORCHAIN", "http://host.docker.internal:1317")
        self.simulation = str2bool(os.getenv("SIMULATION", "true"))
        self.max_profits = str2bool(os.getenv("MAX_PROFITS", "false"))
        self.port = int(os.getenv("PORT", "8000"))

        logging.info(f"THORChain: {self.thorchain}")
        logging.info(f"Simulation: {self.simulation}")
        logging.info(f"Max Profits: {self.max_profits}")
        logging.info(f"Prometheus Port: {self.port}")

        # ensure that while we're not in simulation, we have a mnemonic phrase
        if not self.simulation and os.environ.get("MNEMONIC") is None:
            raise Exception(
                "Must have env var 'MNEMONIC' while not running in simulation mode"
            )
